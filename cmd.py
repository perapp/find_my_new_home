import webcui
import requests
import bs4 as bs
import googlemaps
from urllib.parse import urljoin
import collections

def run():
    objects = []
    objects.extend(parse_bjurfors())

    for obj in objects:
        if obj.type == "Villa":
            print(obj)

def parse_bjurfors(url="https://www.bjurfors.se/sv/tillsalu/vastra-gotaland/molndal/"):
    html_doc = requests.get(url).text
    soup = bs.BeautifulSoup(html_doc, 'html.parser')
    types = {"Friliggande villa": "Villa"}
    attr_keys = {"Adress": "adsress",
                 "Utgångspris": "start_price",
                 "Boarea": "size"}
    for obj in soup.find_all("div", {"class": "m-object"}):
        attrs = {}
        print("="*80)
        attrs["url"] = urljoin(url, obj.a.get("href"))
        attrs["type"] = types.get(obj.p.text)
        for attr_key in obj.find_all("dt"):
            key = attr_keys.get(attr_key.text)
            if not key:
                print(f"Unsupported attr key: {attr_key.text}")
                continue
            attrs[key] = attr_key.find_next_sibling("dd").text
        attrs["pre_show"] = obj.find(string="På förhand") != None
        #print(obj.prettify())
        yield collections.namedtuple("Home", attrs.keys())(**attrs)

if __name__ == "__main__":
    run()
